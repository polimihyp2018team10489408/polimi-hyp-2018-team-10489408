var URL = function () {

    var query_string = {};
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        // If first entry with this name
        if (typeof query_string[pair[0]] === "undefined") {
            query_string[pair[0]] = decodeURIComponent(pair[1]);
            // If second entry with this name
        } else if (typeof query_string[pair[0]] === "string") {
            var arr = [query_string[pair[0]], decodeURIComponent(pair[1])];
            query_string[pair[0]] = arr;
            // If third or later entry with this name
        } else {
            query_string[pair[0]].push(decodeURIComponent(pair[1]));
        }
    }
    return query_string;
}();

var app = angular.module('services', []);
app.controller('servicesController', function ($scope,$http,$q) {
    $scope.locations = [];
    $scope.services = [];
    
     /*var var1 = $http.get("/services")
    .then(function(response) {
        $scope.services = response.data;
    });*/
    
    var var2 = $http.get("/services/locations")
    .then(function(response) {
        $scope.locations = response.data;
        
    });
    
    $q.all([var2]).then( function(){
    
 
    $scope.choice = 0;
    $scope.locationChoice = {
        name: '',
        id: ''
    };
    $scope.locationTitle = '';
    $scope.breadcrumb1 = '';
    $scope.breadcrumb2 = '';
    $scope.none = 'nothing';
    $scope.none1 = 'nothing';
        $scope.ctrl = true;
    if (URL.id != "all" && $scope.choice == 0) {
        $scope.locationChoice.name = $scope.locations[parseInt(URL.id)-1].name;
        $scope.none = '';
        $scope.none1 = '';
        $scope.choice = 1;
        $scope.locationTitle = 'at ' + $scope.locationChoice.name;
        $scope.locationChoice.id = URL.id;
        $scope.breadcrumb1 = "Locations";
        $scope.breadcrumb3 = "Location";
        $scope.breadcrumb2 = "Services in this location";
    }

    
     });
var res = [];
    $scope.selectLocation = function (locationChoice) {
        $scope.ctrl=true;
        $scope.locationChoice = locationChoice;
        $scope.choice = 1;
        $scope.locationTitle = 'at ' + locationChoice.name;
        //if(URL.id=="all")
        // {
        $scope.breadcrumb1 = 'Services';
        $scope.breadcrumb2 = 'Services by a specific Location';
        $scope.none = '';
        $scope.none1 = 'nothing';
        //}
    }
    $scope.reset = function () {
        $scope.ctrl = true;
        $scope.choice = 0;
        $scope.locationChoice = {
        name: '',
        id: ''};
        $scope.locationTitle = '';
        $scope.breadcrumb1 = '';
        $scope.breadcrumb2 = '';
        $scope.none = 'nothing';
        $scope.none1 = 'nothing';
    }
   
    $scope.servicesByChoice = function () {
        if ($scope.choice == 0){
            if($scope.ctrl)
            $http.get("/services")
    .then(function(response) {
        res = response.data;
    }); $scope.ctrl=false;
            return res;
        }
        if ($scope.choice == 1)
            return $scope.servicesByLocation();
    }
    
    $scope.servicesByLocation = function () {
        
        if($scope.ctrl)
        $http.get("/locations/"+$scope.locationChoice.id+"/services")
    .then(function(response) {
        res = response.data;
    });
        $scope.ctrl=false;
        return res;
    }




});