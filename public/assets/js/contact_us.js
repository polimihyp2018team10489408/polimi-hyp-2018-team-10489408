function isFormCompiledCorrectly(formRetrievedInfos) {
    let x = {};
    for (var pair of formRetrievedInfos.entries()) {
        if (pair[1] == null || pair[1] == "") {
            alert("All fields need to be filled before sending the message")
            return false;
        }
    }
    return true;
}

function convertDataToJSON(form) {
    var datas = '{' +
        '"firstname" :"' + document.getElementById("firstName").value + '",' +
        '"lastname" :"' + document.getElementById("secondName").value + '",' +
        '"phone" :"' + document.getElementById("phone").value + '",' +
        '"email" :"' + document.getElementById("email").value + '",' +
        '"message" :"' + document.getElementById("message").value + '"' +
        '}';
    console.log(datas);
    return datas;
}

function onClick() {
    console.log("Clicked");
    var form = new FormData(document.getElementById("form_container"));
    var json = convertDataToJSON(form);
    if (isFormCompiledCorrectly(form)) {
        fetch('/messages', {
            method: "POST",
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            },
            body: json
        }).then(res => {
            if (res.status == 200)
                alert("We'll get to you as soon as possible");
            else
                alert("Something went wrong, retry later");
        });
    }
}