$(document).ready(function () {
  $.getJSON('/people/' + URL.id, function (json) {
    loadPersonMainInfo(json);
  })
});

var serviceElement = "";
var locationElement = "";

function loadPersonServices(json) {
  for (var i = 0; i < json.length - 1; i++) {
    console.log(json);
    serviceElement += '<a href="service.html?id=' + json[i].id + '">' + json[i].title + '</a>, ';
  }
  serviceElement += '<a href="service.html?id=' + json[json.length - 1].id + '">' + json[json.length - 1].title + '</a> ';
  $(".serviceElement").append("Primary service: " + serviceElement);
}

function loadPersonLocations(json) {
  for (var i = 0; i < json.length; i++) {
    locationElement += '<p class="ml-2"><i class="fa fa-hospital-o nonav"></i> Location: <a id="location" style="text-decoration: none">' + json[i].name + '</a><i class="fa fa-map-marker nonav"></i><a>' + json[i].address + '</a></p>';
  }
  $(".locationElement").append(locationElement);
}

var URL = function () {

  var query_string = {};
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i = 0; i < vars.length; i++) {
    var pair = vars[i].split("=");
    if (typeof query_string[pair[0]] === "undefined") {
      query_string[pair[0]] = decodeURIComponent(pair[1]);
    } else if (typeof query_string[pair[0]] === "string") {
      var arr = [query_string[pair[0]], decodeURIComponent(pair[1])];
      query_string[pair[0]] = arr;
    } else {
      query_string[pair[0]].push(decodeURIComponent(pair[1]));
    }
  }
  return query_string;
}();



function loadPersonMainInfo(json) {
  console.log(serviceElement);

  var el = "";
  el = '<h3 id="title" class="mb-4 ml-3">' + json[0].name + '</h3><div class="row"><div class="col-md-4 text-center"><img class="img-fluid rounded mb-4 ml-3" src="../assets/img/people/d00' + json[0].id + '.png" alt=""></div><div class="col-md-8"><p class="ml-3"> ' + json[0].role + '</p><p class="ml-3 serviceElement"></p><div class="locationElement"></div><p class="ml-2"><i class="fa fa-phone"></i> ' + json[0].phone + '</p><p class="ml-2"><i class="fa fa-envelope"></i><a id="mailperson" href="mailto:info@example.com" style="text-decoration: none">' + json[0].mail + '</a></p><p class="ml-3"><a href="#"><i class="fa fa-facebook"></i></a><a href="#"><i class="fa fa-twitter"></i></a><a href="#"><i class="fa fa-linkedin"></i></a></p></div> <div class="col-lg-12"><h3 class="mb-4 ml-3">Personal description:</h3><p class="ml-3 mr-3 text">' + json[0].personalDescription + '<h3 class="mb-4 ml-3">Education:</h3><p class="ml-3 mr-3 text">' + json[0].education + '</p></div></div>';
  $(".COMPLETA").append(el);

  if (URL.s == "none") {
    $(".breadcrumb").append('<ol class="breadcrumb"><li class="breadcrumb-item">All people</li><li class="breadcrumb-item active aria-current="page"">Person</li><li><a href="people.html?id=all"><i class="fa fa-arrow-circle-left"></i></a></li></ol></>');
  } else {
    $(".breadcrumb").append('<ol class="breadcrumb"><li class="breadcrumb-item">Services</li><li class="breadcrumb-item">Service</li><li class="breadcrumb-item">People working in this service</li><li class="breadcrumb-item active aria-current="page"">Person</li><li><a href="people.html?id=' + URL.s + '"><i class="fa fa-arrow-circle-left"></i></a></li></ol></>');
  }

  $.getJSON('/people/' + URL.id + '/services', function (json) {
      loadPersonServices(json);
    }),
    $.getJSON('/people/' + URL.id + '/locations', function (json) {
      loadPersonLocations(json);
    });
}