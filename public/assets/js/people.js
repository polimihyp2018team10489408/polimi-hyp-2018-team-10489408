//WHY CSS NOT FULLY WORKING? BORDERS

$(document).ready(function () {
    if (URL.id == "all") {
        $.getJSON('/people', function (people) {
            loadAllPeople(people);
        });
    } else {
        $.getJSON('/services/' + URL.id, function (service) {
            loadServices(service);
        });

        $.getJSON('/services/' + URL.id + '/people', function (people) {
            loadServicePeople(people);
        });
    }
});

var URL = function () {
    var query_string = {};
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (typeof query_string[pair[0]] === "undefined") {
            query_string[pair[0]] = decodeURIComponent(pair[1]);
        } else if (typeof query_string[pair[0]] === "string") {
            var arr = [query_string[pair[0]], decodeURIComponent(pair[1])];
            query_string[pair[0]] = arr;
        } else {
            query_string[pair[0]].push(decodeURIComponent(pair[1]));
        }
    }
    return query_string;
}();

function loadServices(json) {
    $(".container.text-area").append('<h1 class="display-3">OUR TEAM</h1><h2 class="display-3-bis" >involved in "' + json[0].title + '"</h2>');
}

function loadAllPeople(json) {
    $(".container.text-area").append('<h1 class="display-3">OUR TEAM</h1>');
    var el = "";
    orderJson(json);
    for (var i = 0; i < json.length; i++) {
        el += '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-4"><a href="person.html?id=' + json[i].id + '&s=none" ><div class="card d00' + json[i].id + '"><img class="card-img-top" src="../assets/img/people/d00' + json[i].id + '.png" alt=""><div class="card-body">' + json[i].name + '<div class="team-social"><a href="#"><i class="fa fa-facebook"></i></a><a href="#"><i class="fa fa-twitter"></i></a><a href="#"><i class="fa fa-linkedin"></i></a><p></p><p><i class="fa fa-dot-circle-o"></i>' + json[i].role + '</p><p><i class="fa fa-phone"></i>' + json[i].phone + '</p><p><i class="fa fa-envelope"></i><a id="mailpeople" href="mailto:info@example.com" style="text-decoration: none" "> ' + json[i].mail + '</a></p></div></div></div></a></div>';
    }
    $(".GRIGLIA").append(el);
}

function loadServicePeople(json) {
    var el = "";
    orderJson(json);
    for (var i = 0; i < json.length; i++) {
        el += '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-4"><a href="person.html?id=' + json[i].id + '&s=' + URL.id + '" ><div class="card' + ' ' + json[i].id + '"><img class="card-img-top" src="../assets/img/people/d00' + json[i].id + '.png" alt=""><div class="card-body">' + json[i].name + '<div class="team-social"><a href="#"><i class="fa fa-facebook"></i></a><a href="#"><i class="fa fa-twitter"></i></a><a href="#"><i class="fa fa-linkedin"></i></a><p></p><p><i class="fa fa-dot-circle-o"></i>' + json[i].role + '</p><p><i class="fa fa-phone"></i>' + json[i].phone + '</p><p><i class="fa fa-envelope"></i><a id="mailpeople" href="mailto:info@example.com" style="text-decoration: none" "> ' + json[i].mail + '</a></p></div></div></div></a></div>';
    }

    $(".breadcrumb").append('<ol class="breadcrumb"><li class="breadcrumb-item">Services</li><li class="breadcrumb-item">Service</li><li class="breadcrumb-item active" aria-current="page">People in this service</li><li><a href="service.html?id=' + URL.id + '"><i class="fa fa-arrow-circle-left"></i></a></li></ol></>');

    $(".GRIGLIA").append(el);
}

function orderJson(json) {
    var tmp = 0;
    for (var j = 0; j < json.length; j++) {
        for (var z = 0; z < json.length - j - 1; z++) {
            if (json[z].name > json[z + 1].name) {
                tmp = json[z];
                json[z] = json[z + 1];
                json[z + 1] = tmp;
            }
        }
    }
}