$(document).ready(function () {

    $.getJSON('/locations/' + URL.id, function (json) {
        console.log(json);
        var el = "";

        el = '<div class="jumbotron jumbotron-fluid" id="single-location"><div class="container container-fluid jt-title-container"><div class="row"><div class="col-7"><h1 class="display-4">' + json[0].name + '</h1><p class="lead">' + json[0].address + '</p></div></div><div class="row float-sm-right float-right" id="middle-jumbo-buttons"><div class="col-6"><button type="button" class="btn btn-primary" onclick="scrollToMap()">Show on Maps</button></div><div class="col-6"><button type="button" class="btn btn-primary" id="telephone-number" disabled>Call Us: ' + json[0].phone + '</button></div></div></div></div><nav class="breadcrumb" aria-label="breadcrumb"></nav><br><div class="container" id="container-single-location"><div class="row"><div class="col-sm-12 col-md-7" id="first-col-single-location"><h1 class="display-5">General Infos</h1><hr /><p>' + json[0].description1 + '</p><p>' + json[0].description2 + '</p><a href="services.html?id=' + json[0].id + '" class="aColoredBold">Learn more about the services provided in this location</a></div><div class="col-sm-12 col-md-5" id="second-col-single-location"><div class="text-center"><img src="' + "../assets/img/location/house-" + json[0].id + ".jpg" + '" class="rounded single-location"></img></div></div></div><div class="row"><div class="col-sm-12 col-md-5 mb-5 mt-5"><div class="card"><div class="card-header text-center"><h1>Reviews</h1></div><div class=" card-body "><br>' + '<div class="REVIEWS"></div>' + '</div></div></div><div class="col-sm-12 col-md-7 col-md-5 mb-5 mt-5"><div class ="card"><div class="card-header text-center"><h1>Upcoming Events</h1></div><div class="card-body"><br>' + '<div class="EVENTS"></div>' + '</div></div></div></div></div>';
        $(".COMPLETA").append(el);
        $("title").append(json[0].name);
        google.maps.event.addDomListener(window, 'load', map(json[0].latitude, json[0].longitude));

        if (URL.s == "none") {
            $(".breadcrumb").append('<ol class="breadcrumb"><li class="breadcrumb-item">All locations</li><li class="breadcrumb-item active aria-current="page"">Location</li><li><a href="locations.html?id=all"><i class="fa fa-arrow-circle-left"></i></a></li></ol></>');
        } else {
            $(".breadcrumb").append('<ol class="breadcrumb"><li class="breadcrumb-item">Services</li><li class="breadcrumb-item">Service</li><li class="breadcrumb-item active aria-current="page"">Location providing this service</li><li><a href="service.html?id=' + URL.s + '"><i class="fa fa-arrow-circle-left"></i></a></li></ol></>');
        }
        $.getJSON('/events/' + URL.id, function (jsonEvents) {
                var elEvents = "";
                console.log(jsonEvents);
                for (var i = 0; i < jsonEvents.length; i++) {
                    elEvents += '<a href="#" class="aColored"><h6 class="card-subtitle mb-1">' + jsonEvents[i].name + '</h6></a><p class="card-text ">' + jsonEvents[i].date + '</p>';
                }
                $(".EVENTS").append(elEvents);
            }),

            $.getJSON('/reviews/' + URL.id, function (jsonReviews) {
                console.log(jsonReviews);
                var elRev = "";
                for (var i = 0; i < jsonReviews.length; i++) {
                    elRev += '<h6 class="card-subtitle mb-1 text-muted">' + jsonReviews[i].reviewer + "  " + jsonReviews[i].date + '</h6><p class="card-text ">' + jsonReviews[i].comment + '</p>';
                }
                elRev += '<a href="#" class="aColored"><h6 class="text-center">READ MORE</h6></a>';
                $(".REVIEWS").append(elRev);
            });
    });
})

/* LOADS MAP IN SINGLE LOCATION WITH LATIDTUDE AND LONGITUDE OF THE LOCATION */

function map(latitude, longitude) {
    /* Coordinates to show */
    var var_location = new google.maps.LatLng(latitude, longitude);

    /* Center the red pointer in maps */
    /* How much zoom to show */
    var var_mapoptions = {
        center: var_location,
        zoom: 14
    };

    /* var_map now is the container defined with the map-container ID */
    var var_map = new google.maps.Map(document.getElementById("map-container"),
        var_mapoptions);

    /* Set marker at the specified coordinates in the map-container */
    var var_marker = new google.maps.Marker({
        position: var_location,
        map: var_map,
        title: "Location"
    });
}

function scrollToMap() {
    $("html, body").animate({
        scrollTop: $("#map").offset().top
    }, 2000);
}

/**
 * READS URL AND KEEPS THE LATTER INFOS OF IT, LIKE IDs
 */
var URL = function () {

    var query_string = {};
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        // If first entry with this name
        if (typeof query_string[pair[0]] === "undefined") {
            query_string[pair[0]] = decodeURIComponent(pair[1]);
            // If second entry with this name
        } else if (typeof query_string[pair[0]] === "string") {
            var arr = [query_string[pair[0]], decodeURIComponent(pair[1])];
            query_string[pair[0]] = arr;
            // If third or later entry with this name
        } else {
            query_string[pair[0]].push(decodeURIComponent(pair[1]));
        }
    }
    return query_string;
}();