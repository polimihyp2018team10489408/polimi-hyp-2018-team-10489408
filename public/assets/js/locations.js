$(document).ready(function () {

    // $.ajax({
    //     type:'GET',
    //     url:'/locations',
    //     success: function(result) {
    //         console.log("Successful");

    //     }
    // })

    $.getJSON('/locations', function (data) {
        loadData(data);
    });

});

var URL = function () {

    var query_string = {};
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (typeof query_string[pair[0]] === "undefined") {
            query_string[pair[0]] = decodeURIComponent(pair[1]);
        } else if (typeof query_string[pair[0]] === "string") {
            var arr = [query_string[pair[0]], decodeURIComponent(pair[1])];
            query_string[pair[0]] = arr;
        } else {
            query_string[pair[0]].push(decodeURIComponent(pair[1]));
        }
    }
    return query_string;
}();

function loadData(json) {
    var el = "";

    for (var i = 0; i < 6; i++) {
        // CREATES EACH LOCATION
        console.log(URL.id)
        if (URL.id == "all") {
            el += '<div class="col-sm-12 col-md-6 col-lg-4 mt-4"><div class="card"><img class="card-img-top" src="' + "../assets/img/location/house-" + json[i].id + ".jpg" + '" alt="Card image cap"><div class="card-body"><h4 class="card-title no-margin text-left">' + json[i].name + '</h4><h5 class="card-title text-left">' + json[i].address + '</h5><p class="phoneNumber"><i class="fas fa-microphone-alt"></i>' + json[i].phone + '</p><a <a href="location.html?id=' + json[i].id + '&s=none" class="btn btn-primary location btn-lg btn-block text-left">View care home</a></div></div></div>';
        } else {
            el += '<div class="col-sm-12 col-md-6 col-lg-4 mt-4"><div class="card"><img class="card-img-top" src="' + "../assets/img/location/house-" + json[i].id + ".jpg" + '" alt="Card image cap"><div class="card-body"><h4 class="card-title no-margin text-left">' + json[i].name + '</h4><h5 class="card-title text-left">' + json[i].address + '</h5><p class="phoneNumber"><i class="fas fa-microphone-alt"></i>' + json[i].phone + '</p><a <a href="location.html?id=' + json[i].id + '&s=' + URL.s + '" class="btn btn-primary location btn-lg btn-block text-left">View care home</a></div></div></div>';
        }
    }

    $(".GRIGLIA").append(el);

}