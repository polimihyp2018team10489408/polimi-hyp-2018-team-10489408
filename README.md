# Hypermedia Project 2018

## GENERAL INFORMATIONS
- Heroku URL: https://polimi-hyp-2018-team-10489408.herokuapp.com
- Bitbucket Repo URL: https://bitbucket.org/polimihyp2018team10489408/polimi-hyp-2018-team-10489408/src/master/
- Righetti Mattia (846580) -> ADMINISTRATOR
- Staccone Francesco (847605)
- Scotti Andrea (846630)

# PROJECT STRUCTURE

## How the site has been organized
The project is cointained in the `public` directory as requested.
We decided to make a single CSS file for every single HTML page but the Single Person one and the same apllies for the JavaScript files.

The HTML pages are mostly located in the `public/pages/` folder, the `index.html` is the homepage and it sits directly in the `public` folder.

Footer and Navbar have a single HTML file that we used for each page with the import function provided by Angular JS.

The JSON files used to build the database are in the folder `other/db/`.
We build our database with Knex and we tested it locally by running npm test in the terminal.

## Client-Languages used
- HTML
- CSS
- JavaScript

## Library used
- Jquery

## Framework used
- Angular JS

## Template used
- None

For the desgin part, like colors and shapes, we have taken inspiration from

- http://wrapbootstrap.com/preview/WB059350N
- https://w3layouts.com/child-care-medical-category-bootstrap-responsive-web-template/

# What each member worked on

## Mattia Righetti
- Locations page and its CSS and JS files
- Location page and its CSS and JS files
- Who We Are page and its CSS and JS files
- Contact Us page and its CSS and JS files
- JSON files for database

## Staccone Francesco
- Navbar and its CSS
- Footer and its CSS
- People page and its CSS and JS files
- Person page and its CSS and JS files
- Home page and its CSS and JS files
- Database and REST API

## Scotti Andrea
- Navbar and its CSS
- Services page and its CSS and JS files
- Service page and its CSS and JS files
- Home page and its CSS and JS files
- Database and REST API

# Description of the REST API

- /locations [GET] (retrieve all the locations)
- /locations/:id [GET] (retrieve the location with id as identifier)
- /reviews/:locationId [GET] (retrieve all the reviews linked to the location with locationId as identifier)
- /events/:locationId [GET] (retrieve all the events linked to the location with locatonId as identifier)
- /locations/:locationId/services [GET] (retrieve all the services linked to the location with locatonId as identifier)
- /services [GET] (retrieve all the services)
- /services/locations [GET] (retrieve all the locations but not with all the fields, needed the for the filter)
- /services/:id [GET] (retrieve the service with id as identifier)
- /services/:serviceId/locations [GET] (retrieve all the locations that offer the service with serviceId as identifier)
- /services/:serviceId/people [GET] (retrieve all the people that are involved in the service with serviceId as identifier)
- /people [GET] (retrieve all the people)
- /people/:id [GET] (retrieve the person with id as identifier)
- /people/:personId/locations [GET] (retrieve all the locations where a person with personId as identifier works)
- /people/:personId/services [GET] (retrieve all the services where a person with personId as identifier is involved in)
- /messages [POST] (used to post a new message from contact us)




