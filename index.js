const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const knex = require("knex");
const process = require("process");
const _ = require("lodash");

var path = require("path");
let db;
let serverPort = process.env.PORT || 5000;

let locationsList = require("./other/db/locations.json");
let peopleList = require("./other/db/people.json");
let servicesList = require("./other/db/services.json");
let reviewsList = require("./other/db/reviews.json");
let eventsList = require("./other/db/events.json");
let involvedIn = require("./other/db/involvedIn.json");
let workingAt = require("./other/db/workingAt.json");
let offeredAt = require("./other/db/offeredAt.json");


function initKnex() {

  if (process.env.TEST) {
    db = knex({
      client: "sqlite3",
      debug: true,
      connection: {
        filename: "./other/db/db.sqlite"
      },
      useNullAsDefault: true
    });
  } else {
    db = knex({
      debug: true,
      client: "pg",
      connection: process.env.DATABASE_URL,
      ssl: true
    });
  }
}

function initDb() {

  db.schema.hasTable("services").then(exists => {
    if (!exists) {
      db.schema
        .createTable("services", table => {
          table.increments();
          table.string("title");
          table.text("preview");
        })
        .then(() => {
          return Promise.all(
            _.map(servicesList, s => {
              delete s.id;
              return db("services").insert(s);
            })

          );
        });
    }
  }).then( () =>

  db.schema.hasTable("people").then(exists => {
    if (!exists) {
      db.schema.createTable("people", table => {
          table.increments();
          table.string("name");
          table.string("role");
          table.string("phone");
          table.string("mail");
          table.text("personalDescription");
          table.text("education");
        })
        .then(() => {
          return Promise.all(
            _.map(peopleList, p => {
              delete p.id;
              return db("people").insert(p);
            })
          );
        });
    }
  })).then( () =>

  db.schema.hasTable("locations").then(exists => {
    if (!exists) {
      db.schema
        .createTable("locations", table => {
          table.increments();
          table.string("name");
          table.string("address");
          table.string("phone");
          table.text("description1");
          table.text("description2");
          table.string("latitude");
          table.string("longitude");
        })
        .then(() => {
          return Promise.all(
            _.map(locationsList, l => {
              delete l.id;
              return db("locations").insert(l);
            })
          );
        });
    }
  })).then( () =>

  db.schema.hasTable("reviews").then(exists => {
    if (!exists) {
      db.schema
        .createTable("reviews", table => {
          table.increments();
          table.string("reviewer");
          table.text("comment");
          table.string("date");
          table.integer("locationId").references("id").inTable("locations").notNullable();
        })
        .then(() => {
          return Promise.all(
            _.map(reviewsList, r => {
              delete r.id;
              return db("reviews").insert(r);
            })
          );
        });
    }
  })).then( () =>

  db.schema.hasTable("events").then(exists => {
    if (!exists) {
      db.schema
        .createTable("events", table => {
          table.increments();
          table.string("name");
          table.string("date");
          table.integer("locationId").references("id").inTable("locations").notNullable();
        })
        .then(() => {
          return Promise.all(
            _.map(eventsList, e => {
              return db("events").insert(e);
            })
          );
        });
    }
  })).then( () =>

  db.schema.hasTable("messages").then(exists => {
    if (!exists) {
      db.schema.createTable("messages", table => {
        table.increments();
        table.string("firstname");
        table.string("lastname");
        table.string("phone");
        table.string("mail");
        table.text("message");
      }).then(() => {})
    }
  })).then( () => 

  db.schema.hasTable("offeredAt").then(exists => {
    if (!exists) {
      db.schema
        .createTable("offeredAt", table => {
          table.increments();
          table.integer("locationId").references("id").inTable("locations").notNullable();
          table.integer("serviceId").references("id").inTable("services").notNullable();
        })
        .then(() => {
          return Promise.all(
            _.map(offeredAt, o => {
              return db("offeredAt").insert(o);
            })
          );
        });
    }
  })).then( () =>

  db.schema.hasTable("workingAt").then(exists => {
    if (!exists) {
      db.schema
        .createTable("workingAt", table => {
          table.increments();
          table.integer("personId").references("id").inTable("people").notNullable();
          table.integer("locationId").references("id").inTable("locations").notNullable();
        })
        .then(() => {
          return Promise.all(
            _.map(workingAt, w => {
              return db("workingAt").insert(w);
            })
          );
        });
    }
  })).then( () =>

  db.schema.hasTable("involvedIn").then(exists => {
    if (!exists) {
      db.schema
        .createTable("involvedIn", table => {
          table.increments();
          table.integer("personId").references("id").inTable("people").notNullable();
          table.integer("serviceId").references("id").inTable("services").notNullable();
        })
        .then(() => {
          return Promise.all(
            _.map(involvedIn, i => {
              return db("involvedIn").insert(i);
            })
          );
        });
    }
  }));
}

app.disable('etag');
app.use(function (req, res, next) {
  req.headers['if-none-match'] = 'no-match-for-this';
  next();
});
app.use(express.static(path.join(__dirname, './public')));
app.use('/', express.static(path.join(__dirname, './public/pages')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));


//LOCATIONS
app.get("/locations", function (req, res) {
  db("locations").select('locations.id', 'locations.name', 'locations.address', 'locations.phone').then(result => {
    res.send(JSON.stringify(result));
  });
});
//SINGLE LOCATION
app.get("/locations/:id", function (req, res) {
  let idL = parseInt(req.params.id);
  db('locations').where('id', idL).then(location => {
    res.send(JSON.stringify(location));
  });
});
//REVIEWS FOR THAT LOCATION 
app.get("/reviews/:locationId", function (req, res) {
  let idL = parseInt(req.params.locationId);
  db('reviews').where('locationId', idL).then(reviews => {
    res.send(JSON.stringify(reviews));
  });
});
//EVENTS FOR THAT LOCATION
app.get("/events/:locationId", function (req, res) {
  let idL = parseInt(req.params.locationId);
  db('events').where('locationId', idL).then(events => {
    res.send(JSON.stringify(events));
  });
});

//SERVICES FOR THAT LOCATION
app.get("/locations/:locationId/services", function (req, res) {
  let idL = parseInt(req.params.locationId);
  db('offeredAt').where('locationId', idL).innerJoin('services', 'offeredAt.serviceId', 'services.id').then(services => {
    res.send(JSON.stringify(services));
  });
});

//SERVICES
app.get("/services", function (req, res) {
  db("services").then(result => {
    res.send(JSON.stringify(result));
  });
});
app.get("/services/locations", function (req, res) {
  db("locations").select('id', 'name').then(result => {
    res.send(JSON.stringify(result));
  });
});
//SINGLE SERVICE
app.get("/services/:id", function (req, res) {
  let idS = parseInt(req.params.id);
  db('services').where('id', idS).then(service => {
    res.send(JSON.stringify(service));
  });
});
//LOCATIONS THAT OFFER THAT SERVICE
app.get("/services/:serviceId/locations", function (req, res) {
  let idS = parseInt(req.params.serviceId);
  db('offeredAt').where('serviceId', idS).innerJoin('locations', 'offeredAt.locationId', 'locations.id').select('locations.id', 'locations.name', 'locations.address', 'locations.phone').then(locations => {
    res.send(JSON.stringify(locations));
  });
});
//PEOPLE INVOLVED IN THAT SERVICE
app.get("/services/:serviceId/people", function (req, res) {
  let idS = parseInt(req.params.serviceId);
  db('involvedIn').where('serviceId', idS).innerJoin('people', 'involvedIn.personId', 'people.id').then(people => {
    res.send(JSON.stringify(people));
  });
});

//PEOPLE
app.get("/people", function (req, res) {
  db("people").select('id', 'role', 'phone', 'mail', 'name').then(result => {
    res.send(JSON.stringify(result));
  });
});
//PERSON
app.get("/people/:id", function (req, res) {
  let idP = parseInt(req.params.id);
  db('people').where('id', idP).then(person => {
    res.send(JSON.stringify(person));
  });
});
//LOCATIONS WHERE THAT PERSON WORKS
app.get("/people/:personId/locations", function (req, res) {
  let idP = parseInt(req.params.personId);
  db('workingAt').where('personId', idP).innerJoin('locations', 'workingAt.locationId', 'locations.id').select('locations.name', 'locations.address').then(locations => {
    res.send(JSON.stringify(locations));
  });
});
//SERVICES IN WHICH THAT PERSON IS INVOLVED
app.get("/people/:personId/services", function (req, res) {
  let idP = parseInt(req.params.personId);
  db('involvedIn').where('personId', idP).innerJoin('services', 'involvedIn.serviceId', 'services.id').select('services.id', 'services.title').then(services => {
    res.send(JSON.stringify(services));
  });
});
// FORM POST REQUEST
app.post("/messages", function (req, res) {
  let newMessage = {
    firstname: req.body.firstname,
    lastname: req.body.lastname,
    phone: req.body.phone,
    mail: req.body.email,
    message: req.body.message
  };
  db('messages').insert(newMessage).then(function () {
    res.json({
      success: true,
      message: 'ok'
    });
  });
});

/* Uncomment this to test the post
app.get("/messages", function (req, res) {
  db("messages").then(result => {
    res.send(JSON.stringify(result));
  });
});*/

app.set("port", serverPort);
initKnex();
initDb();

/* Start the server on port 5000 */
app.listen(serverPort, function () {
  console.log(`Your app is ready at port ${serverPort}`);

});
